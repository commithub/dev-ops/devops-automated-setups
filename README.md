# devops-automated-setups

Bash scripts utilized for automating system setups.

## License

This project is licensed under the AGPL-3.0-or-later License - see the [LICENSE.md](LICENSE.md) file for details

