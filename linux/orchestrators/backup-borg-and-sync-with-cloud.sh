#!/bin/bash

# Backups the borgbackup repo and then uses rclone to sync to cloud provider
# @param {string} borg_repo - Path to borg repository
# @param {array} directories_to_backup - list of all the directories to backup
# @param {string} name_of_storage - rclone storage name
# @param {string} bucket_name - Cloud hosted bucket name
# @example
#   ./backup-borg-and-sync-with-cloud.sh \
#     /home/usr/borg-repo \
#     ("/media/sda1/dir1" "/media/sda1/dir2") \
#     b2 \
#     b2-backups

# Exit when any command fails with an error message
set -e

# PARAMS
borg_repo=$1
directories_to_backup=$2
name_of_storage=$3
bucket_name=$4

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/../modules/message.sh"
prune_borg_backups="$location_dir/../modules/prune-borg-backups.sh"
borg_backup_directories="$location_dir/../modules/borg-backup-directories.sh"
rclone_sync_borg="$location_dir/../modules/rclone-sync-borg.sh"

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap '$message "\"${last_command}\" command filed with exit code $?."' EXIT

$message "Starting backup process"
$message "Sourcing secrets..."
source "$location_dir/.secrets.sh"
$message "Secrets found"

$borg_backup_directories $borg_repo $directories_to_backup $borg_passphrase;
$prune_borg_backups $borg_repo $borg_passphrase;
$rclone_sync_borg $borg_repo $name_of_storage $bucket_name;

$message "finished backup process"
