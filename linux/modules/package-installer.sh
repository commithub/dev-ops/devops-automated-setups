#!/bin/bash

# Pass a list of packages for the package manager to install
# @param {array} packages - list of all the packages you want to install
# @example
#   ./package-installer.sh ("vim" "git")

# PARAMS
packages=$1

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"
package_manager=$("$location_dir/get-system-package-manager.sh")
install_keyword="install -y"

if [ $package_manager == "pacman" ]
then
  unset install_keyword
 install_keyword="-Syu"
fi

$message "installing packages"

for i in $packages; do
  sudo $package_manager $install_keyword $i
done

$message "done installing packages"
