#!/bin/bash

# borgbackup script to keep backups clean
# @param {string} borg_repo - path to the borg repo
# @param {string} password - password for borg repo
# @example
#   ./prune-borg-backups.sh /home/usr/borg-repo

# PARAMS
borg_repo=$1
password=$2

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"

$message "Pruning borg backups"

export BORG_PASSPHRASE=$password

borg prune \
  --stats \
  --keep-daily 14 \
  --keep-weekly 4 \
  --keep-monthly 6 \
  --keep-yearly -1 \
  --save-space \
  $borg_repo

$message "borg backups have been pruned"
