#!/bin/bash

# Installation and setup for Gnome Boxes

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "gnome-boxes"

