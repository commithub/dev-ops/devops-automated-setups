#!/bin/bash

# Install and setup node

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh
PACKAGE_MANAGER=$(~/devops-automated-setups/linux/modules/get-system-package-manager.sh)

# Setup
cd ~

if [ $PACKAGE_MANAGER == "apt" ]
then
  # Install an up-to-date version of Node with NodeSource
  # https://github.com/nodesource/distributions/blob/master/README.md
  curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
fi

$PACKAGE_INSTALLER "nodejs"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | sudo bash
