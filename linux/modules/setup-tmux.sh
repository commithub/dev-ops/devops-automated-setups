#!/bin/bash

# Install and setup tmux

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "tmux"
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
cd ~
cat > .tmux.conf <<- "EOF"
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'

# Free the original Ctrl-b prefix keybinding
unbind-key C-b

# Setting the prefix to Ctrl-a
set-option -g prefix C-a

# Send Prefix
bind-key C-a send-prefix

#setting the delay between prefix and command
set -s escape-time 5

# Set bind key to reload configuration file
# prefix + r
bind r source-file ~/.tmux.conf \; display "Reloaded!"

# Use Alt-arrow keys to switch panes
# Alt + Directional arrow
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# Shift arrow to switch windows
# Shift + Directional arrow
bind -n S-Left previous-window
bind -n S-Right next-window

# Mouse Mode
set -g mouse on

# Set easier window split keys
# prefix + v for vertical
# prefix + h for horizontal
bind-key v split-window -v
bind-key h split-window -h

# Default to vi
set -g status-keys vi

# Copy Paste
# prefix + [ to enter copy mode

# Use mouse for copy mode
set-option -g mouse on
# Use vim keybindings in copy mode
setw -g mode-keys vi
set-option -s set-clipboard off
# prefix + P for paste
bind P paste-buffer
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi y send-keys -X rectangle-toggle
unbind -T copy-mode-vi Enter
bind-key -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel 'xclip -se c -i'
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xclip -se'

# Set the status line's colors
set -g status-style fg=white,bg=blue

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
EOF
