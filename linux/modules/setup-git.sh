#!/bin/bash

# Installation and setup for Git

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "git-all"
git config --global user.name $USER
git config --global user.email "${$USER}@commithub.com"
git config --global core.editor vim
git config --global init.defaultBranch main


