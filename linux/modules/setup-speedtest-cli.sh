#!/bin/bash

# Installation and setup for speedtest-cli
# @returns {string}
# @example
#   bash setup-speedtest-cli.sh

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"
packager_installer="$location_dir/package-installer.sh"
package_manager=$("$location_dir/get-system-package-manager.sh")

# Exit when any command fails with an error message
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap '$message "\"${last_command}\" command filed with exit code $?."' EXIT

if [ $package_manager == "dnf" ]
then
  $message "downloading rpm package..."
  curl -s https://install.speedtest.net/app/cli/install.rpm.sh | sudo bash
elif [ $package_manager == "apt" ]
then
  $message "downloading deb package..."
  curl -s https://install.speedtest.net/app/cli/install.deb.sh | sudo bash
else
  $message "current Linux distribution is not supported" | exit 0
fi

$message "download completed"
$message "installing speedtest"
$package_installer "speedtest"
$message "installation done"
