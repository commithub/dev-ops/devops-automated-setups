#!/bin/bash

# Installation and setup for inkscape

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "inkscape"

