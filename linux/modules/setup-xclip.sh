#!/bin/bash

# Installation and setup for xclip

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh

# Setup
$PACKAGE_INSTALLER "xclip"

