#!/bin/bash

# Run speedtest-cli securely and export to JSON
# If needed when running as a cron job source the .bash profile
# @param {string} logs_dir - the directory where you want to store the log
# @returns {string}
# @example
#   bash run-speedtest.sh ~/logs/speedtest

#PARAMS
logs_dir=$1

# LOCAL
location_dir="$(dirname $(realpath $0))"
message="$location_dir/message.sh"
file_name="$(hostname)-$(date -Iseconds | cut -d '+' -f 1).json"

# Exit when any command fails with an error message
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap '$message "\"${last_command}\" command filed with exit code $?."' EXIT

$message "Running speedtest..."
speedtest --json --secure > "$logs_dir/$file_name"
$message "Tests completed and logs have been stored"
