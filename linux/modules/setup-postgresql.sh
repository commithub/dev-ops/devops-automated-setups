#!/bin/bash

# Install and setup postgresql
# Refer to the following docs on more info for setups:
# https://fedoraproject.org/wiki/PostgreSQL
# https://wiki.archlinux.org/title/PostgreSQL
# https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-20-04

# Imports
PACKAGE_INSTALLER=~/devops-automated-setups/linux/modules/package-installer.sh
PACKAGE_MANAGER=$(~/devops-automated-setups/linux/modules/get-system-package-manager.sh)

# Setup
if [$PACKAGE_MANAGER == "dnf" ]
then
  $PACKAGE_INSTALLER "postgresql-server postgresql-contrib"
  sudo postgresql-setup initdb
elif [$PACKAGE_MANAGER == "pacman"]
then
  $PACKAGE_INSTALLER "postgresql"
  initdb -D /var/lib/postgres/data
elif [$PACKAGE_MANAGER == "apt"]
then
  $PACKAGE_INSTALLER "postgresql postgresql-contrib"
fi

# Create postgres user with super user privileges
sudo -u postgres createuser $USER -s
sudo -u postgres createdb $USER
sudo systemctl start postgresql
# Run at boot
sudo systemctl enable postgresql

