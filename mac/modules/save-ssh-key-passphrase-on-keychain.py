import os

try:
    config = """'# Same identity file and configuration for any domain
Host *
AddKeysToAgent yes
UseKeychain yes'"""

    os.system("echo %s > ~/.ssh/config"%config)
except:
    exit("Failed to create identity configuration file for the ssh key passphrase")

